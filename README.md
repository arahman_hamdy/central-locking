## Central Blocking System ##

The repository contains the restful central blocking system files required by [CodeScalers](http://www.codescalers.com) as a test 

The implementation depended upon the redlock algorithm proposed by [REIDS - Distributed Lock](https://redis.io/topics/distlock)

**The project contains:**
- Simple web application using bottle and redis as memory database for saving locks

### Bottle Version
0.12.13

### Boddle Version (for running tests)
0.2.3

### Installation

Clone the repo to your local machine
```
git clone [repo_url] seechic
```

Install redis
```
$ sudo apt-get install redis-server
```

Install python requirments
```
$ sudo pip install -r requirments.txt
```

Run the server
```
cd locking
python main.py
```


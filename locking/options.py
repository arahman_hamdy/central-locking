"""
The options needed by red-lock manager to handle multiple redis instances
"""

CONNECTION_LIST = [
    {
        'host': 'localhost',
        'port': 6379,
        'db': 0
    }
]

RETRY_COUNT = 3
RETRY_DELAY = 0.2
DRIFT_FACTOR = 0.01

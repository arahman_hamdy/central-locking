import uuid
import time
from collections import namedtuple

import redis
from redis.exceptions import RedisError

import options

Lock = namedtuple("Lock", ("validity", "resource", "code"))


class LockManager(object):
    def __init__(self):
        """
        @summary: The constructor for the locking manager to set the redis servers list connections
        :return:
        """
        self.servers = []
        for connection in options.CONNECTION_LIST:
            if isinstance(connection, basestring):
                server = redis.StrictRedis.from_url(connection)
            elif type(connection) == dict:
                server = redis.StrictRedis(**connection)
            else:
                server = connection
            self.servers.append(server)
        self.minimum_connections = (len(options.CONNECTION_LIST) // 2) + 1

    @staticmethod
    def lock_instance(server, resource, value, ttl):
        """
        @summary: this method is responsible for locking a resource
        in a single redis instance
        :param server: the redis server instance
        :param resource: the resource name
        :param value: the unique code for this lock
        :param ttl: time to live
        :return:
        """
        return server.set(resource, value, nx=True, px=ttl)

    @staticmethod
    def unlock_instance(server, resource, value):
        """
        @summary: this method is responsible for deleting lock to release a resource
        in a single redis instance
        :param server: the redis server instance
        :param resource: the resource name
        :param value: the unique code for this lock
        :return:
        """
        if server.get(resource) == value:
            return server.delete(resource)
        return False

    @staticmethod
    def get_unique_id():
        """
        @summary: Get a unique identifier to be used as lock code
        :return the unique identifier for the lock:
        """
        return str(uuid.uuid4())

    def lock(self, resource, ttl):
        """
        @summary: this method is responsible for locking a resource
        in all redis instances
        :param resource: the resource name
        :param ttl: time to live
        :return: the lock namedtuple object if success or list of errors if failed
        """
        retry = 0
        code = self.get_unique_id()

        # Add 2 milliseconds to the drift to account for Redis expires
        # precision, which is 1 millisecond, plus 1 millisecond min
        # drift for small TTLs.
        drift = int(ttl * options.DRIFT_FACTOR) + 2
        errors = []
        while retry < options.RETRY_COUNT:
            n = 0
            start_time = int(time.time() * 1000)
            for server in self.servers:
                try:
                    if self.lock_instance(server, resource, code, ttl):
                        n += 1
                except RedisError as e:
                    if str(e) not in errors:
                        errors.append(str(e))
            elapsed_time = int(time.time() * 1000) - start_time
            validity = int(ttl - elapsed_time - drift)
            if validity > 0 and n >= self.minimum_connections:
                return Lock(validity, resource, code)
            else:
                # unlock any previously succeeded locking
                self.unlock(resource, code)
                retry += 1
                time.sleep(options.RETRY_DELAY)
        return errors

    def unlock(self, resource, code):
        """
        @summary: this method is responsible for deleting lock to release a resource
        in all redis instances
        :param resource: the resource name
        :param code: the unique code for the resource lock
        :return:
        """
        n = 0
        errors = []
        for server in self.servers:
            try:
                if self.unlock_instance(server, resource, code):
                    n += 1
            except RedisError as e:
                if str(e) not in errors:
                    errors.append(str(e))
        if n < self.minimum_connections:
            errors.append("Failed to delete the resource lock")
        return errors

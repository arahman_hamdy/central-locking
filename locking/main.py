from bottle import route, run, response

from redlock import LockManager


@route('/acquire/<resource>/<ttl:int>', method='GET')
def lock_acquire(resource, ttl):
    """
    @summary Responsible for handling acquiring locks requests
    :param resource(string): the resource name
    :param ttl(integer): time to live in ms - the time needed by the process to consume the resource
    :return: the response with the lock details if success or the error details if failed
    """

    lock_manager = LockManager()
    result = lock_manager.lock(resource, ttl)
    # check if result contains errors list
    if not isinstance(result, list):
        response.status = 200
        response.body = {
            "success": True,
            "lock": {
                "validity": result.validity,
                "resource": result.resource,
                "code": result.code
            }
        }
    elif result:
        response.status = 400
        response.body = {
            "success": False,
            "error": ", ".join(result)
        }
    else:
        response.status = 504
        response.body = {
            "success": False,
            "error": "The resource you are trying to access is locked - Timeout Error"
        }
    return response.body


@route('/release/<resource>/<code>', method='DELETE')
def lock_release(resource, code):
    """
    @summary: Responsible for unlocking the resources by deleting its key
    :param resource:
    :param code (string): the lock code got from the acquiring lock handler
    :return: the response body with no content if success or the error details if failed
    """
    lock_manager = LockManager()
    result = lock_manager.unlock(resource, code)
    if not result:
        response.status = 204
    else:
        response.status = 400
        response.body = {"success": False, 'errors': ", ".join(result)}
    return response.body


run(host='localhost', port=8080, debug=True)

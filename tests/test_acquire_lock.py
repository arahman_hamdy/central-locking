import unittest
from boddle import boddle
from locking.main import lock_acquire

params = {
    'resource': 'resource_1',
    'ttl': 200
}


class TestAcquireLock(unittest.TestCase):
    def test_acquire_resource(self):
        with boddle():
            # acquiring resource for the first time should be success = True
            self.assertEqual(lock_acquire(**params)['success'], True)
            # acquiring resource for the second time should be success = False because
            # the resource is locked
            self.assertEqual(lock_acquire(**params)['success'], False)

    def test_acquire_resource_code(self):
        with boddle():
            self.assertIn('lock', lock_acquire(**params))
            self.assertIn('code', lock_acquire(**params)['lock'])

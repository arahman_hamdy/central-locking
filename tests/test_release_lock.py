import unittest
from boddle import boddle
from locking.main import lock_acquire, lock_release

params = {
    'resource': 'resource_1',
    'ttl': 20000
}


class TestReleaseLock(unittest.TestCase):
    def test_release_resource_with_valid_code(self):
        with boddle():
            lock = lock_acquire(**params)['lock']
            # deleting resource using valid code
            release_params = {
                'resource': lock['resource'],
                'code': lock['code']
            }
            self.assertTrue(lock_release(**release_params)['success'])

    def test_release_resource_with_invalid_code(self):
        with boddle():
            lock = lock_acquire(**params)['lock']
            # deleting resource using valid code
            release_params = {
                'resource': lock['resource'],
                'code': False
            }
            self.assertFalse(lock_release(**release_params)['success'])
